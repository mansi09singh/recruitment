import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor(private httpClient: HttpClient) { }

  getTag():  Observable<any>{
    return this.httpClient.get("https://www.tronalddump.io/tag");
  }

  getQuotes(data): Observable<any>{
     return this.httpClient.get("https://www.tronalddump.io/search/quote?tag="+ data)
  }
  
}