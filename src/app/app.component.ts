/*
** @author mansi.singh
** This component displays 
** TAGS & QUOTES  for the selected tags.
*/

import { Component, OnInit } from '@angular/core';
import { MatExpansionPanel } from '@angular/material/expansion';
import { TagService } from 'src/tag.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  public arrKey: any[]=[];
  public quotesArr = new Array;
  public panelOpenState: boolean;

  constructor(public tagService: TagService){}

  ngOnInit(){
    this.getTags();
  }

  getTags(){
    this.tagService.getTag().subscribe((res)=>{
      res._embedded.tag.forEach(element => {
        let key= element.value;
        this.arrKey.push(key) ;  
      }); 
    });
  }


  getQuotes(data){
    if(data!=null){
      this.tagService.getQuotes(data).subscribe((res)=>{
        this.quotesArr= new Array;
         res._embedded.quotes.forEach(element=>{
           this.quotesArr.push(element.value)
      });
    });
  }
  }
  
returnTags(){
  return this.arrKey;
  }
returnQuotes(){
  return this.quotesArr;
}
}