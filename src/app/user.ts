export class user {
    constructor(
        public name: string,
        public email: string,
        public phone: number,
        public timePreference: string,
        public topic: string,
        public subscribe: boolean,

    ){}
}
